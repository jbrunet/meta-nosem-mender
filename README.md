# meta-nosem

This README file contains information on the contents of the meta-nosem-mender
layer.

It provides basic mender support for meta-nosem

Please see the corresponding sections below for details.

## Dependencies

* poky: `git://git.yoctoproject.org/poky` - refspec: `dunfell`
* meta-mender: `https://github.com/mendersoftware/meta-mender.git` - refspec: `dunfell`
* meta-nosem: `https://gitlab.com/jbrunet/meta-nosem.git` - refspec: `dunfell`

## Patches

Please submit any patches against the meta-nosem layer through a pull
request to the corresponding gitlab project at
https://gitlab.com/jbrunet/meta-nosem-mender

## Maintainer

Jerome Brunet <jbrunet@baylibre.com>

## Quick Start with Kas

This layer has basic for support kas (see
https://kas.readthedocs.io/en/1.0/index.html) Boards are
provided with the related kas yaml file.

Just checkout `meta-nosem-mender` and fire up `kas`. It will take care of the
dependencies and the configuration

```
git clone https://<whereverthisis>/meta-nosem-mender.git
kas build kas/amlogic-p241.yml
```

This will build `core-image-base` for the `amlogic-p241`

## Flashing

refer to meta-nosem and meta-mender for the the flashing instruction