FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}/${PV}:"

SRC_URI_append = " \
	file://0001-MENDER-arm64-meson-config-p241-trim-options.patch \
	file://0002-MENDER-arm64-meson-config-p241-amend-configuration-f.patch \
	file://0003-MENDER-arm64-meson-add-mender_bootcmd-to-meson64-con.patch"

MENDER_UBOOT_AUTO_CONFIGURE_amlogic-p241 = "0"
BOOTENV_SIZE_amlogic-p241 = "0x10000"
MENDER_UBOOT_ENV_STORAGE_DEVICE_OFFSET_1_amlogic-p241 = "0x1f0000"
MENDER_UBOOT_ENV_STORAGE_DEVICE_OFFSET_2_amlogic-p241 = "0x170000"

