# Enable all Mender feature in a convinient way for nosem

_MENDER_IMAGE_TYPE_DEFAULT ?= "mender-image-sd"
_MENDER_BOOTLOADER_DEFAULT ?= "mender-uboot"

MENDER_IMAGE_BOOTLOADER_BOOTSECTOR_OFFSET ?= "1"
MENDER_MBR_BOOTLOADER_LENGTH ?= "440"

inherit mender-full

# Make sure the atf is built when we need it
do_image_sdimg[depends] += " ${@bb.utils.contains('SOC_FAMILY', 'amlogic', 'virtual/trusted-firmware-a:do_populate_sysroot', '', d)}"
